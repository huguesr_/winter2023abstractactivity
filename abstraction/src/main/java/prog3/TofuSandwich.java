package prog3;

public class TofuSandwich extends VegetarianSandwich {
    private String filling;

    public TofuSandwich(){
        this.filling = "Tofu";
    }

    public String getProtein(){
        return "Tofu";
    }

    public String getFilling(){
        return this.filling;
    }
}
