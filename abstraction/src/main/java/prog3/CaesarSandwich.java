package prog3;

public class CaesarSandwich extends VegetarianSandwich{
        private String filling;
    public CaesarSandwich(){
        this.filling = "Caesar dressing";
    }

    @Override
    public String getProtein(){
        return "Anchovies";
    }

    // @Override
    // public boolean isVegetarian(){
    //     return false;
    // }

    public String getFilling(){
        return this.filling;
    }
}
