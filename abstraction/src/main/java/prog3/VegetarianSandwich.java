package prog3;

abstract class VegetarianSandwich implements ISandwich{

    private String filling;

    public VegetarianSandwich(){
        this.filling = "";
    }

    public abstract String getProtein();

    public void addFilling(String topping){
        String [] bad = {"Chicken","beef","fish","meat","pork"};
        for(int i = 0;i<bad.length;i++){
            if(topping == bad[i]){
                throw new UnsupportedOperationException("MEAT ALERT!!!");
            }
        }
        this.filling = topping;
    }

    public final boolean isVegetarian(){
        return true;
    }

    // public String getFilling(){
    //     return this.filling;
    // }

    public boolean isVegan(){
        if (this.filling.equals("cheese") || this.filling.equals("egg")){
            return false;
        }
        return true;
    }


    
}
