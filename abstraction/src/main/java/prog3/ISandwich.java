package prog3;

public interface ISandwich {
    String getFilling();

    void addFilling(String topping);

    boolean isVegetarian();
}
