package prog3;

public class App {
   public static void main(String[] args){
    ISandwich is = new TofuSandwich();
    if(is instanceof ISandwich){
        System.out.println("It is");
    }
    if(is instanceof VegetarianSandwich){
        System.out.println("Vegetarian");
    }

    //is.addFilling("Chicken");
    //is.isVegan(); does not compile
    VegetarianSandwich vg = (VegetarianSandwich)is;
    System.out.println(vg.isVegan());

    VegetarianSandwich cs = new CaesarSandwich();
    System.out.println(cs.isVegetarian());
   }
}
