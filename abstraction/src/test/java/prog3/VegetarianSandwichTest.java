package prog3;
import static org.junit.Assert.*;

import org.junit.Test;
public class VegetarianSandwichTest {
    @Test
    public void testAddFilling(){
        VegetarianSandwich vs = new VegetarianSandwich();
        try{
            vs.addFilling("Chicken");
            fail("did not throw error");
        }
        catch (Exception e){
            System.out.println("exception thrown");
        }

    }

    @Test
    public void testIsVegetarian(){
        VegetarianSandwich vs = new VegetarianSandwich();
        if(vs.isVegetarian()){
        }
        else {
            fail("Returned false or nothing");
        }

    }

    @Test
    public void testGetFilling(){
        VegetarianSandwich vs = new VegetarianSandwich();
        vs.addFilling("veg");
        assertEquals("veg", vs.getFilling());
    }
}
