package prog3;
import static org.junit.Assert.*;

import org.junit.Test;

public class BagelSandwichTest {

    @Test
    public void tryBagelGetFilling()
    {
        BagelSandwich bS = new BagelSandwich();
        bS.addFilling("letuce");
        
        assertEquals("letuce", bS.getFilling());
    }

    @Test public void tryAddFilling(){
        BagelSandwich bS = new BagelSandwich();
        
        try{
            bS.addFilling("sadsa");
        }
        catch (Exception e){
            fail("Exception should've been caught");
        }
    }

    @Test public void tryIsVegetarian(){
        BagelSandwich bS = new BagelSandwich();
        
        try{
            bS.isVegetarian();
            fail("Exception should've been caught");
        }
        catch (Exception e){
            System.out.println("gg");
        }
    }
    
}
